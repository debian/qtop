%define ver      2.3.4
%define rel      0
%define prefix   /usr

Summary: running processes control
Name: Top
Version: %{ver}
Release: %{rel}
License: GPL
Group: User Interface/X
Source: %{name}-%{ver}.tar.gz
BuildRoot: /var/tmp/%{name}-buildroot

%description
running processes control

%prep
%setup -q -n %{name}-%{ver} %{rel}

%build
cmake -DCMAKE_INSTALL_PREFIX=%{prefix} .
make -j4

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc COPYING INSTALL
%{prefix}/bin/Top
%changelog
