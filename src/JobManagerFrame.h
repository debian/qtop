#ifndef JobManagerFrame_h
#define JobManagerFrame_h

/******************************************************************************
*
* Copyright (C) 2002 Hugo PEREIRA <mailto: hugo.pereira@free.fr>
*
* This is free software; you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation; either version 2 of the License, or (at your option) any later
* version.
*
* This software is distributed in the hope that it will be useful, but WITHOUT
* Any WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "Counter.h"
#include "Debug.h"
#include "Key.h"

#include <QCloseEvent>
#include <QWidget>

class JobManager;
class SummaryFrame;

//* customized QVBox to recieve close event
class JobManagerFrame: public QWidget, private Base::Counter<JobManagerFrame>, public Base::Key
{

    Q_OBJECT

    public:

    //* constructor
    explicit JobManagerFrame( QWidget* = nullptr );

    //* jobManager
    JobManager& jobManager() const
    { return *jobManager_; }

    //* summary frame
    SummaryFrame& summaryFrame() const
    { return *summaryFrame_; }

    //* set user (convenience function)
    void setUser( const QString& ) const;

    private:

    //* job manager
    JobManager* jobManager_ = nullptr;

    //* summary frame
    SummaryFrame* summaryFrame_ = nullptr;

};

#endif
