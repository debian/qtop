#ifndef JobManager_h
#define JobManager_h

/******************************************************************************
*
* Copyright (C) 2002 Hugo PEREIRA <mailto: hugo.pereira@free.fr>
*
* This is free software; you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation; either version 2 of the License, or (at your option) any later
* version.
*
* This software is distributed in the hope that it will be useful, but WITHOUT
* Any WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "JobCommand.h"
#include "Job.h"
#include "JobModel.h"
#include "TimeStamp.h"

#include <QWidget>

class ToolTipWidget;
class TreeView;

/**
\class JobManager
\brief Job managment singleton
*/
class JobManager:public QWidget
{

    //* Qt meta object declaration
    Q_OBJECT

    public:

    //* constructor
    explicit JobManager( QWidget* );

    //* list
    TreeView& list() const
    { return *treeView_; }

    //* resume selected jobs
    void resume();

    //* pause selected jobs
    void pause();

    //* kill selected jobs
    void kill();

    //* tree view
    bool toggleTreeView( bool );

    //* process job list
    void processJobList( Job::Set );

    //* jobs
    const Job::List& jobs() const
    { return jobs_; }

    //* retrieve selected jobs
    Job::List selectedJobs() const;

    //* user
    QString user() const
    { return user_; }

    //* tree view
    bool treeViewEnabled() const
    { return treeViewEnabled_; }

    //* update user
    bool setUser( QString );

    protected Q_SLOTS:

    //* show tooltip
    void _showToolTip( const QModelIndex& );

    protected:

    //* update model
    void _updateModel();

    private Q_SLOTS:

    //* update configuration
    void _updateConfiguration();

    private:

    //* display list
    TreeView* treeView_ = nullptr;

    //* tooltip
    ToolTipWidget* toolTipWidget_ = nullptr;

    //* show icons
    bool showIcons_ = false;

    //* show full names
    bool showFullNames_ = false;

    //* true if tree view is selected
    bool treeViewEnabled_ = false;

    //* current user
    QString user_;

    //* current set of processes
    Job::List jobs_;

    //* job model
    JobModel model_;

};

#endif
