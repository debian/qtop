/******************************************************************************
*
* Copyright (C) 2002 Hugo PEREIRA <mailto: hugo.pereira@free.fr>
*
* This is free software; you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation; either version 2 of the License, or (at your option) any later
* version.
*
* This software is distributed in the hope that it will be useful, but WITHOUT
* Any WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "JobRecord.h"

#include "Debug.h"

//_____________________________________________________
bool JobRecord::updateJob( const Job& job )
{
    Debug::Throw( "JobRecord::updateJob.\n" );
    if( job.id() != job_.id() ) return false;

    // update job
    job_.updateFrom( job );

    // update current record
    current_ = Record( job_ );

    // update max record
    max_.cpu_ = qMax( max_.cpu_, current_.cpu_ );
    max_.memory_ = qMax( max_.memory_, current_.memory_ );

    // update samples
    records_.add( current_ );

    return true;
}

//_____________________________________________________
JobRecord::Record::Record( const Job& job ):
    Counter( "Record" ),
    virtualMemory_( job.virtualMemory() ),
    residentMemory_( job.residentMemory() ),
    sharedMemory_( job.sharedMemory() ),
    cpu_( job.cpu() ),
    memory_( job.memory() )
{ Debug::Throw( "JobRecord::Record::Record.\n" ); }

//___________________________________________
void JobRecord::Samples::setSampleSize( int size )
{
    Debug::Throw() << "JobRecord::Samples::setSampleSize - " << size << endl;
    sampleSize_ = size;
    while( sampleList_.size() > sampleSize_ )
    { sampleList_.removeFirst(); }
}

//________________________________________________
void JobRecord::Samples::setMaxLength( int length )
{
    Debug::Throw() << "JobRecord::Samples::setMaxLength: " << length << endl;
    maxLength_ = length;
    while( recordList_.size() > maxLength_ )
    { recordList_.removeFirst(); }
}

//________________________________________________
void JobRecord::Samples::add( const JobRecord::Record& record )
{

    // add data to samples
    sampleList_ << record;

    // check size
    if( sampleList_.size() > sampleSize_ )
    {

        sampleList_.removeFirst();

        Record sum;
        for( const auto& record:sampleList_ )
        { sum += record; }

        sum /= sampleSize_;
        recordList_ << sum;
        if( recordList_.size() > maxLength_ ) recordList_.removeFirst();

    }

    return;

}
