/******************************************************************************
*
* Copyright (C) 2002 Hugo PEREIRA <mailto: hugo.pereira@free.fr>
*
* This is free software; you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation; either version 2 of the License, or (at your option) any later
* version.
*
* This software is distributed in the hope that it will be useful, but WITHOUT
* Any WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/
#include "SummaryFrame.h"

#include "GridLayout.h"
#include "Debug.h"
#include "File.h"
#include "UserSet.h"
#include "Util.h"

#include <QTextStream>

//____________________________________________________________
SummaryFrame::SummaryFrame( QWidget* parent ):
    DockPanel( parent )
{

    Debug::Throw( "SummaryFrame::SummaryFrame.\n" );

    // dock panel title
    setTitle( tr( "Summary" ) );
    setOptionName( "SUMMARY_FRAME" );

    GridLayout* gridLayout = new GridLayout;
    gridLayout->setMargin( 0 );
    gridLayout->setSpacing( 5 );
    gridLayout->setMaxCount( 2 );
    gridLayout->setColumnAlignment( 0, Qt::AlignRight|Qt::AlignVCenter );
    panel().layout()->addItem( gridLayout );

    gridLayout->addWidget( new QLabel( tr( "Processes:" ), &panel() ) );
    gridLayout->addWidget( jobs_ = new QLabel( &panel() ) );

    gridLayout->addWidget( new QLabel( tr( "CPU:" ), &panel() ) );
    gridLayout->addWidget( totalCpu_ = new QProgressBar( &panel() ) );
    totalCpu_->setMaximum( 100 );
    totalCpu_->setSizePolicy( QSizePolicy::Minimum, QSizePolicy::Minimum );

    gridLayout->addWidget( new QLabel( tr( "Memory:" ), &panel() ) );
    gridLayout->addWidget( totalMemory_ = new QProgressBar( &panel() ) );
    totalMemory_->setMaximum( 100 );
    totalMemory_->setSizePolicy( QSizePolicy::Minimum, QSizePolicy::Minimum );

}

//____________________________________________________________
void SummaryFrame::updateJobs( const Job::List& jobs )
{
    Debug::Throw( "SummaryFrame::update.\n" );

    QString user( userName_ );

    // number of running jobs
    {
        int jobCount( jobs.size() );
        jobs_->setText( QString( "%1" ).arg( jobCount ) );

        summary_ = QString( tr( "Processes: %1 " ) ).arg( jobCount );

    }

    // total cpu
    {
        double totalCpu( 0 );
        for( const auto& job:jobs )
        { totalCpu += job.cpu(); }
        totalCpu_->setValue( qMin( int(totalCpu), totalCpu_->maximum() ) );
        summary_ += QString( tr( "CPU: %1% " ) ).arg( qMin( int(totalCpu), totalCpu_->maximum() ) );
    }

    // total mem
    {
        double totalMemory( 0 );
        for( const auto& job:jobs )
        { totalMemory += job.memory(); }
        totalMemory_->setValue( qMin( int(totalMemory), totalMemory_->maximum() ) );
        summary_ += QString( tr( "Memory: %1% " ) ).arg( qMin( int(totalMemory), totalMemory_->maximum() ) );
    }

}

//____________________________________________________________
void SummaryFrame::setUser( QString user )
{
    Debug::Throw( "SummaryFrame::setUser.\n" );
    if( userName_ == user ) return;

    userName_ = user;
    QString host( Util::host() );
    if( host == "localhost" ) setTitle( user );
    else {
        QString buffer;
        QTextStream( &buffer ) << user << " [" << host << "]";
        setTitle( QString( "%1 [%2]" ).arg( user ).arg( host ) );
    }

}
