#ifndef Menu_h
#define Menu_h

/******************************************************************************
*
* Copyright (C) 2002 Hugo PEREIRA <mailto: hugo.pereira@free.fr>
*
* This is free software; you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation; either version 2 of the License, or (at your option) any later
* version.
*
* This software is distributed in the hope that it will be useful, but WITHOUT
* Any WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "Counter.h"

#include <QMenuBar>

//! main menu
class Menu:public QMenuBar, private Base::Counter<Menu>
{

    Q_OBJECT

    public:

    //! creator
    explicit Menu( QWidget* parent );

    private Q_SLOTS:

    //! get list of editor windows into menu
    void _updateWindowsMenu();

    private:

    //! Windows windows menu
    QMenu* windowsMenu_;

};

#endif
