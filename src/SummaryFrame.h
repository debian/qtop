#ifndef SummaryFrame_h
#define SummaryFrame_h

/******************************************************************************
*
* Copyright (C) 2002 Hugo PEREIRA <mailto: hugo.pereira@free.fr>
*
* This is free software; you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation; either version 2 of the License, or (at your option) any later
* version.
*
* This software is distributed in the hope that it will be useful, but WITHOUT
* Any WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "Counter.h"
#include "CustomComboBox.h"
#include "DockPanel.h"
#include "Job.h"
#include "JobManager.h"

#include <QLabel>
#include <QProgressBar>

class SummaryFrame: public DockPanel
{

    //! Qt meta object declaration
    Q_OBJECT

    public:

    //! constructor
    explicit SummaryFrame( QWidget* parent );

    //! summary
    const QString& summaryString() const
    { return summary_; }

    //! update from job list
    void updateJobs( const Job::List& );

    //! update user list
    void setUser( QString user );

    private:

    //! user
    QString userName_;

    //! jobs label
    QLabel* jobs_;

    //! total cpu usage
    QProgressBar* totalCpu_;

    //! total memory usage
    QProgressBar* totalMemory_;

    //! information string
    QString summary_;

};

#endif
