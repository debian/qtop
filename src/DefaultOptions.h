#ifndef DefaultOptions_h
#define DefaultOptions_h

/******************************************************************************
*
* Copyright (C) 2002 Hugo PEREIRA <mailto: hugo.pereira@free.fr>
*
* This is free software; you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation; either version 2 of the License, or (at your option) any later
* version.
*
* This software is distributed in the hope that it will be useful, but WITHOUT
* Any WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "Color.h"
#include "File.h"
#include "Job.h"
#include "Util.h"
#include "XmlOptions.h"

//_____________________________________________________
//* default options installer
void installDefaultOptions()
{

    XmlOptions::get().keep( "USER_NAME" );

    XmlOptions::get().setAutoDefault( true );

    XmlOptions::get().set<int>( "DB_SIZE", 10 );
    XmlOptions::get().set( "DIV_COLOR", Base::Color( "#cccccc" ) );
    XmlOptions::get().set<int>( "DIV_X", 0 );
    XmlOptions::get().set<int>( "DIV_Y", 5 );
    XmlOptions::get().set( "FOREGROUND_COLOR", Base::Color( "#aa0000" ) );
    XmlOptions::get().set<int>( "MAIN_WINDOW_HEIGHT", 300 );
    XmlOptions::get().set<int>( "MAIN_WINDOW_WIDTH", 492 );

    XmlOptions::get().set<int>( "RECORD_LENGTH", 1024 );
    XmlOptions::get().set<int>( "RECORD_DIALOG_WIDTH", 300 );
    XmlOptions::get().set<int>( "RECORD_DIALOG_HEIGHT", 300 );
    XmlOptions::get().set<int>( "REFRESH_RATE", 2 );

    XmlOptions::get().set<int>( "SAMPLES", 5 );
    XmlOptions::get().set<int>( "TRANSPARENCY_FOREGROUND_INTENSITY", 204 );
    XmlOptions::get().set<int>( "TRANSPARENCY_LOAD_INTENSITY", 128 );
    XmlOptions::get().set<bool>( "TRANSPARENCY_USE_GRADIENT", true );
    XmlOptions::get().set<bool>( "RECORD_ALL_JOBS", true );

    XmlOptions::get().set<bool>( "TREE_VIEW", false );
    XmlOptions::get().set<bool>( "SHOW_SUMMARY", true );
    XmlOptions::get().set<bool>( "SHOW_TOOLTIPS", true );
    XmlOptions::get().set<bool>( "SHOW_ICONS", true );
    XmlOptions::get().set<bool>( "SHOW_FULL_NAMES", true );

    // run-time non recordable options
    XmlOptions::get().set( "OLD_RC_FILE", Option( File(".Toprc").addPath( Util::home() ), Option::Flag::None ) );
    XmlOptions::get().set( "RC_FILE", Option( File("qtoprc").addPath( Util::config() ), Option::Flag::None ) );

    XmlOptions::get().setAutoDefault( false );

};

#endif
