#ifndef SignalMenu_h
#define SignalMenu_h

/******************************************************************************
*
* Copyright (C) 2002 Hugo PEREIRA <mailto: hugo.pereira@free.fr>
*
* This is free software; you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation; either version 2 of the License, or (at your option) any later
* version.
*
* This software is distributed in the hope that it will be useful, but WITHOUT
* Any WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "Counter.h"

#include <QAction>
#include <QMenu>
#include <QHash>

class SignalMenu: public QMenu, private Base::Counter<SignalMenu>
{

    Q_OBJECT

    public:

    //* constructor
    explicit SignalMenu( QWidget* = nullptr );

    //* signal name matching given id
    QString signalName( int ) const;

    Q_SIGNALS:

    void signalRequested( int );

    protected Q_SLOTS:

    //* request signal
    void _requestSignal( QAction* );

    private:

    //* action map
    using ActionMap = QHash< QAction*, int>;
    ActionMap actions_;

};

#endif
