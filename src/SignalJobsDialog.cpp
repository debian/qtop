/******************************************************************************
*
* Copyright (C) 2002 Hugo PEREIRA <mailto: hugo.pereira@free.fr>
*
* This is free software; you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation; either version 2 of the License, or (at your option) any later
* version.
*
* This software is distributed in the hope that it will be useful, but WITHOUT
* Any WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "SignalJobsDialog.h"

#include "Debug.h"
#include "IconNames.h"
#include "IconEngine.h"
#include "TreeView.h"
#include "XmlOptions.h"

#include <sys/types.h>
#include <signal.h>

//__________________________________________________
SignalJobsDialog::SignalJobsDialog( QWidget* parent, int signal, Job::List jobs ):
    QuestionDialog( parent )
{

    Debug::Throw( "SignalJobsDialog::SignalJobsDialog.\n" );
    setWindowTitle( tr( "Send Signal - Top" ) );

    Q_ASSERT( !jobs.empty() );

    if( jobs.size() == 1 )
    {

        switch( signal )
        {
            case SIGSTOP: setText( QString( tr( "Suspend process named '%1' (%2)?" ) ).arg( jobs.front().name() ).arg( jobs.front().id() ) ); break;
            case SIGCONT: setText( QString( tr( "Resume process named '%1' (%2)?" ) ).arg( jobs.front().name() ).arg( jobs.front().id() ) ); break;
            case SIGINT: setText( QString( tr( "Interrupt process named '%1' (%2)?" ) ).arg( jobs.front().name() ).arg( jobs.front().id() ) ); break;
            case SIGTERM: setText( QString( tr( "Terminate process named '%1' (%2)?" ) ).arg( jobs.front().name() ).arg( jobs.front().id() ) ); break;
            case SIGKILL: setText( QString( tr( "Kill process named '%1' (%2)?" ) ).arg( jobs.front().name() ).arg( jobs.front().id() ) ); break;
            default: setText( QString( tr( "Send signal %1 to process named '%2' (%3)?" ) ).arg( signal ).arg( jobs.front().name() ).arg( jobs.front().id() ) ); break;
        }

    } else {

        switch( signal )
        {
            case SIGSTOP: setText( QString( tr( "Suspend %1 processes ?" ) ).arg( jobs.size() ) ); break;
            case SIGCONT: setText( QString( tr( "Resume %1 processes ?" ) ).arg( jobs.size() ) ); break;
            case SIGINT: setText( QString( tr( "Interrupt %1 processes ?" ) ).arg( jobs.size() ) ); break;
            case SIGTERM: setText( QString( tr( "Terminate %1 processes ?" ) ).arg( jobs.size() ) ); break;
            case SIGKILL: setText( QString( tr( "Kill %1 processes ?" ) ).arg( jobs.size() ) ); break;
            default: setText( QString( tr( "Send signal %1 to %2 processes ?" ) ).arg( signal ).arg( jobs.size() ) ); break;
        }

        TreeView* treeView = new TreeView( this );
        setDetails( treeView );
        treeView->setModel( &model_ );
        model_.set( jobs );

        // mask
        int mask(
            (1<<JobModel::Id )|
            (1<<JobModel::Name ));
        treeView->setMask( mask );
        treeView->resizeColumns();

        treeView->setOptionName( "SIGNAL_JOBS_LIST" );
        treeView->sortByColumn( JobModel::Id, Qt::AscendingOrder );

    }

    // rename buttons
    switch( signal )
    {
        case SIGSTOP:
        okButton().setText( QString( tr( "Suspend" ) ) );
        okButton().setIcon( IconEngine::get( IconNames::Pause ) );
        break;

        case SIGCONT:
        okButton().setText( QString( tr( "Resume" ) ) );
        okButton().setIcon( IconEngine::get( IconNames::Resume ) );
        break;

        case SIGINT:
        okButton().setText( QString( tr( "Interrupt" ) ) );
        okButton().setIcon( IconEngine::get( IconNames::Kill ) );
        break;

        case SIGTERM:
        okButton().setText( QString( tr( "Terminate" ) ) );
        okButton().setIcon( IconEngine::get( IconNames::Kill ) );
        break;

        case SIGKILL:
        okButton().setText( QString( tr( "Kill" ) ) );
        okButton().setIcon( IconEngine::get( IconNames::Kill ) );
        break;

        default: okButton().setText( QString( tr( "Send" ) ) ); break;
    }
    adjustSize();

}
