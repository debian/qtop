#ifndef UserSet_h
#define UserSet_h

/******************************************************************************
*
* Copyright (C) 2002 Hugo PEREIRA <mailto: hugo.pereira@free.fr>
*
* This is free software; you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation; either version 2 of the License, or (at your option) any later
* version.
*
* This software is distributed in the hope that it will be useful, but WITHOUT
* Any WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "Counter.h"

#include <QString>
#include <QStringList>

// user priority class
class UserSet final: private Base::Counter<UserSet>
{
    public:

    //* universal constructor
    template<typename... Args>
    explicit UserSet(Args&&... args):
        Counter( "UserSet" ),
        users_( std::forward<Args>(args)... )
    {}

    //*@name accessors
    //@{

    //* convert
    const QStringList& get() const
    { return users_; }

    //* contains
    bool contains( const QString& value ) const
    { return users_.contains( value ); }

    //* true if given user is all
    static bool isAllUsers( const QString& value )
    { return value == AllUsers; }

    //* all users string
    static const QString& allUsers()
    { return AllUsers; }

    //@}

    //*@name modifiers
    //@{

    //* streamer
    template<class T>
    UserSet& operator<< ( const T& t )
    {
        users_ << t;
        return *this;
    }

    //@}

    private:

    //* users
    QStringList users_;

    //* all users
    static const QString AllUsers;

};

#endif
