/******************************************************************************
*
* Copyright (C) 2002 Hugo PEREIRA <mailto: hugo.pereira@free.fr>
*
* This is free software; you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation; either version 2 of the License, or (at your option) any later
* version.
*
* This software is distributed in the hope that it will be useful, but WITHOUT
* Any WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "JobManagerFrame.h"
#include "JobManager.h"
#include "SummaryFrame.h"

//___________________________________________
JobManagerFrame::JobManagerFrame( QWidget* parent ):
QWidget( parent ),
Counter( "JobManagerFrame" )
{
    Debug::Throw( "JobManagerFrame::JobManagerFrame.\n" );

    // horizontal layout for summary frame and JobManager
    QHBoxLayout *hLayout( new QHBoxLayout );
    hLayout->setMargin(0);
    hLayout->setSpacing(2);
    setLayout( hLayout );

    // sumary frame in a vertical layout
    QVBoxLayout *leftLayout( new QVBoxLayout );
    leftLayout->setMargin(0);
    leftLayout->setSpacing(2);
    hLayout->addLayout( leftLayout );
    leftLayout->addWidget( summaryFrame_ = new SummaryFrame( this ), 0 );
    leftLayout->addStretch( 1 );

    // jobManager
    hLayout->addWidget( jobManager_ = new JobManager( this ), 1 );

}

//________________________________________________________________________
void JobManagerFrame::setUser( const QString& value ) const
{
    jobManager_->setUser( value );
    summaryFrame_->setUser( value );
}
